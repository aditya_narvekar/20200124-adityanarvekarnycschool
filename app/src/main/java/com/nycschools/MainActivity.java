package com.nycschools;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ListView listView;
    ImageView splashimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //setting the status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#0A436F"));
        }

        //setting the properties of fadeout animation of image and text
        final Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(500);
        fadeOut.setDuration(1000);

        //setting up the ui
        setupUI();

        //accessing the database and fetching data
        DatabaseAccess1 databaseAccess = DatabaseAccess1.getInstance(MainActivity.this);
        databaseAccess.open();
        List<School> schooldatalist = databaseAccess.getSchools();
        databaseAccess.close();

        //setting up the adapter
        SchoolAdapter SA = new SchoolAdapter(MainActivity.this, schooldatalist);
        listView.setAdapter(SA);

        //splash screen animation
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                splashimg.startAnimation(fadeOut);
                splashimg.setVisibility(View.GONE);
            }
        }, 2000);


    }

    private void setupUI() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = findViewById(R.id.listView);
        splashimg = findViewById(R.id.splashimg);
    }

}
