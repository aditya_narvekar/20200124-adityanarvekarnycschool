package com.nycschools;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAccess2 {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess2 instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess2(Context context) {
        this.openHelper = new DatabaseOpenHelper2(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess1.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess2 getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess2(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Gets schoolsat info from the database.
     *
     * @return a schoolsat
     */
    public SchoolSat getSchoolSat(String selectedDbn) {
        SchoolSat schoolSat = null;
        Cursor cursor = database.rawQuery("SELECT * FROM satres WHERE Column1 = '" + selectedDbn + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            schoolSat = new SchoolSat(cursor.getString(0), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
            cursor.moveToNext();
        }
        cursor.close();
        return schoolSat;
    }
}
