package com.nycschools;

public class SchoolSat {
    public String dbn;
    public String number_of_sattakers;
    public String sat_critical;
    public String sat_math;
    public String sat_writing;

    public SchoolSat(String dbn, String number_of_sattakers, String sat_critical, String sat_math, String sat_writing) {
        this.dbn = dbn;
        this.number_of_sattakers = number_of_sattakers;
        this.sat_critical = sat_critical;
        this.sat_math = sat_math;
        this.sat_writing = sat_writing;
    }

    public String getDbn() {
        return dbn;
    }

    public String getNumber_of_sattakers() {
        return number_of_sattakers;
    }

    public String getSat_reading() {
        return sat_critical;
    }

    public String getSat_math() {
        return sat_math;
    }

    public String getSat_writing() {
        return sat_writing;
    }
}
