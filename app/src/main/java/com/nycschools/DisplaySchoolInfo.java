package com.nycschools;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import java.util.Objects;

public class DisplaySchoolInfo extends AppCompatActivity {

    TextView schooltextname, optext, locationtext, phonenumber, locateus, emailtext;
    TextView readingtestsc, mathtestsc, writingsc, numberoftesttakersc;
    LinearLayout calllinearlay, mapslinearlay, emaillinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displayschool);

        //setting the status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#0A436F"));
        }

        //animation for activity migration
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        //getting the dbn from previous activity
        Intent i = getIntent();
        String dbn_code = Objects.requireNonNull(i.getExtras()).getString("dbncode");

        //setting up the ui
        setupUI();

        //accessing the db1 and fetching the school data
        DatabaseAccess1 databaseAccess = DatabaseAccess1.getInstance(DisplaySchoolInfo.this);
        databaseAccess.open();
        final School fetchedSchool = databaseAccess.getSchoolInfo(dbn_code);
        databaseAccess.close();

        //accessing the db2 and fetching the school sat data
        DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(DisplaySchoolInfo.this);
        databaseAccess2.open();
        SchoolSat schoolSat = databaseAccess2.getSchoolSat(dbn_code);
        databaseAccess.close();

        //handling the condition if the school sat data doesn't exist in excel
        if (schoolSat != null) {
            numberoftesttakersc.setText(schoolSat.getNumber_of_sattakers());
            mathtestsc.setText(schoolSat.getSat_math());
            readingtestsc.setText(schoolSat.getSat_reading());
            writingsc.setText(schoolSat.getSat_writing());
        }

        //tailoring the strings for setting up to textviews or other functionality
        String address = fetchedSchool.getLocation().split("\\(")[0];
        final String phone = fetchedSchool.getPhone_number();
        final String email = fetchedSchool.getSchool_email();

        final float latitude = Float.parseFloat(fetchedSchool.getLatitude());
        final float longitude = Float.parseFloat(fetchedSchool.getLongitude());

        //setting text to textviews
        schooltextname.setText(fetchedSchool.getSchool_name());
        optext.setText(fetchedSchool.getOverview_paragraph());
        locationtext.setText("Address:\n" + address);
        phonenumber.setText(phone);
        emailtext.setText(fetchedSchool.getSchool_email());

        //setting click listeners for click functionality.
        schooltextname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String website = fetchedSchool.getWebsite();
                if (!website.startsWith("http://") && !website.startsWith("https://"))
                    website = "http://" + website;
                try{
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                    startActivity(browserIntent);
                }
                catch (Exception e){
                    Toast.makeText(DisplaySchoolInfo.this, "Some Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        calllinearlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phone));
                startActivity(intent);
            }
        });

        mapslinearlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        emaillinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SENDTO);
                i.setData(Uri.parse("mailto:"+email));
                i.putExtra(Intent.EXTRA_SUBJECT, "School Information Request");
                startActivity(Intent.createChooser(i, "School Info"));
            }
        });

    }

    private void setupUI() {
        schooltextname = findViewById(R.id.schoolnametxt);
        optext = findViewById(R.id.optxt);
        locationtext = findViewById(R.id.locationtxt);
        phonenumber = findViewById(R.id.phonenotxt);
        emailtext = findViewById(R.id.emailnametext);
        locateus = findViewById(R.id.locateus);

        numberoftesttakersc = findViewById(R.id.numberoftesttakers);
        mathtestsc = findViewById(R.id.mathtestscores);
        readingtestsc = findViewById(R.id.readingtestscores);
        writingsc = findViewById(R.id.writingtestscores);

        calllinearlay = findViewById(R.id.calllinear);
        mapslinearlay = findViewById(R.id.maplinear);
        emaillinear = findViewById(R.id.emaillinearlay);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.blackarrow);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
