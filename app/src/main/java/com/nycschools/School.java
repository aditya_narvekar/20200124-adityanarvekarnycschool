package com.nycschools;

public class School {
    public int serialnumber;
    public String dbn;
    public String school_name;
    public String boro;
    public String overview_paragraph;
    public String shared_space;
    public String campus_name;
    public String location;
    public String phone_number;
    public String fax_number;
    public String school_email;
    public String website;
    public String total_students;
    public String time;
    public String graduation_rate;
    public String latitude;
    public String longitude;

    //Constructor for fetching data according to selected school in 2nd activity
    public School(String dbn, String school_name, String boro, String overview_paragraph, String shared_space, String campus_name, String location, String phone_number, String fax_number, String school_email, String website, String total_students, String time, String graduation_rate, String latitude, String longitude) {
        this.dbn = dbn;
        this.school_name = school_name;
        this.boro = boro;
        this.overview_paragraph = overview_paragraph;
        this.shared_space = shared_space;
        this.campus_name = campus_name;
        this.location = location;
        this.phone_number = phone_number;
        this.fax_number = fax_number;
        this.school_email = school_email;
        this.website = website;
        this.total_students = total_students;
        this.time = time;
        this.graduation_rate = graduation_rate;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    //constructor for MainActivity
    public School(int serialnumber, String dbn, String school_name) {
        this.serialnumber = serialnumber;
        this.dbn = dbn;
        this.school_name = school_name;
    }

    public int getSerialNumber() {
        return serialnumber;
    }

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public String getBoro() {
        return boro;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public String getShared_space() {
        return shared_space;
    }

    public String getCampus_name() {
        return campus_name;
    }

    public String getLocation() {
        return location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getFax_number() {
        return fax_number;
    }

    public String getSchool_email() {
        return school_email;
    }

    public String getWebsite() {
        return website;
    }

    public String getTotal_students() {
        return total_students;
    }

    public String getTime() {
        return time;
    }

    public String getGraduation_rate() {
        return graduation_rate;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
