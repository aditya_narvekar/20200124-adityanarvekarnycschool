package com.nycschools;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DatabaseOpenHelper1 extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "schools.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseOpenHelper1(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
