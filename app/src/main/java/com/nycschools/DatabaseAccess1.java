package com.nycschools;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess1 {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess1 instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess1(Context context) {
        this.openHelper = new DatabaseOpenHelper1(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess1.
     *
     * @param context the Context
     * @return the instance of DatabaseAccess
     */
    public static DatabaseAccess1 getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess1(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all schools from the database.
     *
     * @return a List of schools
     */
    public List<School> getSchools() {
        int serialnumber = 1;
        List<School> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT dbn,school_name FROM schools", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new School(serialnumber++, cursor.getString(0), cursor.getString(1)));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public School getSchoolInfo(String dbncode) {
        School school = null;
        Cursor cursor = database.rawQuery("SELECT * FROM schools WHERE dbn='" + dbncode + "'", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            school = new School(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(13), cursor.getString(14), cursor.getString(15), cursor.getString(16), cursor.getString(17), cursor.getString(18), cursor.getString(19), cursor.getString(20), cursor.getString(21) + " - " + cursor.getString(22), cursor.getString(23), cursor.getString(28), cursor.getString(29));
            cursor.moveToNext();
        }
        cursor.close();
        return school;
    }

}
