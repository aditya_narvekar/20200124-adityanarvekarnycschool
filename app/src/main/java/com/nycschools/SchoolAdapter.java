package com.nycschools;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class SchoolAdapter extends BaseAdapter {

    private Context context;
    private List<School> schoollist;

    public SchoolAdapter(Context context, List<School> data) {
        this.schoollist = data;
        this.context = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return schoollist.get(position);
    }

    @Override
    public int getCount() {
        return schoollist.size();
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        view = null;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.schooltemplate, null);
            holder.serialnumbertext = (TextView) view.findViewById(R.id.serialno);
            holder.schoolnametext = (TextView) view.findViewById(R.id.schoolname);
            holder.mainLin = (LinearLayout) view.findViewById(R.id.mainLinear);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.serialnumbertext.setText(String.valueOf(schoollist.get(position).getSerialNumber()));
        holder.schoolnametext.setText(schoollist.get(position).getSchool_name());
        holder.mainLin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DisplaySchoolInfo.class);
                intent.putExtra("dbncode", schoollist.get(position).getDbn());
                context.startActivity(intent);
            }
        });
        return view;

    }

    class ViewHolder {
        TextView serialnumbertext, schoolnametext;
        LinearLayout mainLin;
    }
}
